# Wardrobify

Team:

* Boris Veits - Hats microservice
* Danny Gomez - Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Models:
Hats - Got the required fields with location as foreign key.
LocationVO - that has an id and import_href
The integration is through the poller which sends the api request to locations api to get all the information about locations.
