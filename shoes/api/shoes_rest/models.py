from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    id = models.IntegerField(primary_key=True)


class Shoes(models.Model):
    """
    The Shoes model describes the manufacturer, model, color
    and provideds a picture of the shoe
    """

    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
    )

    # def get_api_url(self):
    #     return reverse("api_show_location", kwargs={"pk": self.pk})

    # def __str__(self):
    #     return self.name

    # class Meta:
    #     ordering = ("name",)  # Default ordering for Location
